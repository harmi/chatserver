/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Matti
 */
public class SocketMaker {

    private ServerSocket serverSocket;

    public SocketMaker(int port) throws IOException {
        serverSocket = new ServerSocket(port, 5);
        System.out.println("New Server Socket created, port " + serverSocket.getLocalPort());

    }

    public Socket connectionReceiver() throws IOException {
        Socket socket = serverSocket.accept();
        return socket;

    }
}
