/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

import java.util.ArrayList;

/**
 *
 * @author Matti
 */
public class History {

    private ArrayList<ChatObserver> inputHandlerList = new ArrayList();
    private ArrayList<String> chatHistory = new ArrayList();
    private static final History history = new History();

    private History() {
    }

    public static History getHistory() {
        return history;
    }

    public void register(InputHandler inputHandler) {
        inputHandlerList.add(inputHandler);
        System.out.println("Added new input handler to the list " + inputHandler);
    }

    public void addLine(String line) {
        System.out.println("Line " + line + " received in History addLine.");
        chatHistory.add(line);
        System.out.println("Added line " + line + " to history.");
        for (ChatObserver o : inputHandlerList) {
            System.out.println("Sending line " + line + " from history to Input handler to be printed");
            o.chatEntry(line);
        }
    }

    public void getChatHistory(int amountOfHistoryRequested, InputHandler newHandler) {
        if (amountOfHistoryRequested > chatHistory.size()) {
            amountOfHistoryRequested = chatHistory.size();
        }

        for (int i = chatHistory.size() - amountOfHistoryRequested; i < chatHistory.size(); i++) {
            newHandler.chatEntry(chatHistory.get(i));

        }
        
        System.out.println("Finished printing history chat lines for new user.");
    }
}
