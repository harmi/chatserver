/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matti
 */
public class Connection implements Runnable {

    private int port = 0;
    private String ipAddress = "";
    private String userInfo = "";
    private String userInput = "";
    private String userName = "";
    private BufferedReader in;
    private BufferedReader stdIn;
    private InputHandler inputHandler;
    private PrintWriter out;
    private Socket socket;

    public Connection(Socket socket) throws IOException {
        this.socket = socket;
        //prints to user screen
        out = new PrintWriter(this.socket.getOutputStream());
        //reads user input
        in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        stdIn = new BufferedReader(new InputStreamReader(System.in));

        this.port = this.socket.getPort();
        this.ipAddress = this.socket.getInetAddress().toString();
        this.userInfo = this.ipAddress + ":" + this.port;

        System.out.println(userInfo);
        // welcomeText();
        //setUsername();

        inputHandler = new InputHandler(this.userInfo, this.userName, socket);
        inputHandler.newClientConnected("New client connected " + this.userInfo);
    }

    @Override
    public void run() {
        welcomeText();
        //setUsername();
        int i = 0;
        while (true) {
                try {
                    this.userInput = in.readLine();
                    i++;
                    //checking if the userInput was null or not
                    if (this.userInput != null) {
                        System.out.println("Message received from " + this.socket.getInetAddress().toString());
                        System.out.println("New input received. " + i);
                        System.out.println("Passing line to inputAnalyzer");
                        inputHandler.inputAnalyzer(userInput);
                        out.flush();
                    } else {
                        System.out.println("The userinput was " + this.userInput);
                        System.out.println(this.socket.getInetAddress().toString());
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);

                }

        }
    }

    private void setUsername() throws IOException {
        while (true) {
            //asking for username
            out.print("Enter username: ");
            out.flush();
            userInput = in.readLine();
            //checking if userInput is null
            if (userInput != null) {
                //if username is less than 10chars it's accepted
                if (userInput.length() > 10) {
                    out.println("Input a username of 1-10 chars");
                    out.flush();
                } else {
                    this.userName = userInput;

                    System.out.println(this.userName);
                    break;
                }
            } else {
                System.out.println("Something went wrong.");
            }
        }
    }

    private void welcomeText() {
        out.println("Telnet server by 1400389");
        out.println("Type /help for commands");

    }
}
